# Yuwen_IDS721_mini8

## Project Overview

This project is a Rust command-line tool for data ingestion and processing. It aims to provide a convenient way to handle data by automating the process through a command-line interface. The tool will allow users to ingest data from various sources, perform data processing operations, and generate meaningful insights.

The main features of this project include:

- Data ingestion: The tool will support ingesting data from different file formats such as CSV, JSON, and XML. It will provide options to specify the source file or directory and handle large datasets efficiently.

- Data processing: The tool will offer a range of data processing operations such as filtering, sorting, aggregating, and transforming data. Users will be able to apply these operations based on their specific requirements.

- Unit tests: The project will include a comprehensive suite of unit tests to ensure the correctness and reliability of the implemented functionalities. This will help in identifying and fixing any issues or bugs during the development process.

By using this Rust command-line tool, users will be able to streamline their data ingestion and processing tasks, saving time and effort. It will provide a flexible and scalable solution for handling data in various scenarios.

### Steps

To use this Rust command-line tool, follow these steps:

1. Create the repository in your local machine:
    ```
    cargo new <project_name> -- bin
    ```

    The --bin flag is used to specify that the project being created is a binary project, meaning it will produce an executable file. This is in contrast to a library project (--lib), which would produce a library that can be used by other projects.


2. Navigate to the project directory:
    ```
    cd <project_name>
    ```

3. Modify `main.rs` and add dependencies in `Cargo.toml`:
    ```
    csv = "1.1.6"
    ```


4. Create `tests/test.rs` for testing functionality and build the project:
    ```
    cargo build
    ```

5. Run the tool with the desired command and options:
    ```
    cargo run <file_path>
    ```

    Replace `<file_path>` with the specific data source location.

    ![run](imgs/run.png){width=700 px}

6. Run the unit tests to ensure the correctness of the implemented functionalities:
    ```cargo test --test test```
    ![test](imgs/test.png){width=700 px}

7. Generate test report
    ```cargo test > test_report.txt```
    ![test](imgs/test_report.png){width=700 px}
    [report](rust_cli_proj/test_report.txt)