use std::fs::File;
use std::io::{self, BufRead};


// Import necessary modules

// Function to read the CSV file
fn read_csv_file(file_path: &str) -> io::Result<Vec<String>> {
    let file = File::open(file_path)?;
    let reader = io::BufReader::new(file);
    let mut lines = Vec::new();

    for line in reader.lines() {
        lines.push(line?);
    }

    Ok(lines)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_read_csv_file() {
        let file_path = "random.csv";
        let lines = read_csv_file(file_path).unwrap();

        // Add your assertions here to test the contents of the CSV file
        assert_eq!(lines.len(), 6);
        assert_eq!(lines[1], "1001,John Doe,35,Engineering,75000,2015-06-15");
        // Add more assertions as needed
    }
}