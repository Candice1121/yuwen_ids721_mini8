use std::env;
use std::error::Error;
use std::fs::File;
use csv::Reader;


fn main() -> Result<(), Box<dyn Error>> {
    // Read the command line arguments
    let args: Vec<String> = env::args().collect();
    
    // Check if the CSV file path is provided
    if args.len() < 2 {
        eprintln!("Please provide the path to the CSV file as a command line argument.");
        return Ok(());
    }
    
    // Get the CSV file path from the command line arguments
    let csv_file_path = &args[1];
    
    // Open the CSV file
    let file = File::open(csv_file_path)?;
    
    // Create a CSV reader
    let mut reader = Reader::from_reader(file);
    
    // Process each record in the CSV file
    for result in reader.records() {
        let record = result?;
        
        // Process the record as needed
        println!("{:?}", record);
    }
    
    println!("Read successfully");
    
    Ok(())
}
